#pragma once

//---------------------------------------
// Std
//---------------------------------------
#include <vector>
#include <memory>

//---------------------------------------
// Forward Declarations
//---------------------------------------
class IComponent;

namespace ecs
{
	//---------------------------------------
	// Type Defines
	//---------------------------------------
	using EntityUID = unsigned int;

	//---------------------------------------
	class Entity
	{
	public:
		Entity();

		// Don't allow any copying.
		Entity(const Entity& rhs) = delete;
		Entity& operator=(const Entity& rhs) = delete;

		bool AddComponent(std::shared_ptr<IComponent>& component);
		bool RemoveComponent(std::shared_ptr<IComponent>& component);

	private:
		std::vector<std::shared_ptr<IComponent>> m_components;
		EntityUID m_uid;
		static EntityUID UID_COUNTER;
	};


	//---------------------------------------
	// Static Initializations
	//---------------------------------------
	EntityUID Entity::UID_COUNTER = 0;
}
