#pragma once

#include "../interfaces/IComponent.h"
#include "../utilities/Autolist.h"

//---------------------------------------
using namespace utility;

namespace ecs
{
	class RenderC : public IComponent, public Autolist<RenderC>
	{
	public:
		virtual void Init();
		virtual void Tick();
		virtual void Destroy();
	};
}