#include "Entity.h"

#include <algorithm>

namespace ecs
{
	//---------------------------------------
	Entity::Entity() : m_uid(UID_COUNTER++)
	{}

	//---------------------------------------
	bool Entity::AddComponent(std::shared_ptr<IComponent>& component)
	{
		auto it = std::find(m_components.begin(), m_components.end(), component);
		if (it != m_components.end())
		{
			return false;
		}

		m_components.push_back(component);

		return true;
	}

	//---------------------------------------
	bool Entity::RemoveComponent(std::shared_ptr<IComponent>& component)
	{
		auto it = std::find(m_components.begin(), m_components.end(), component);
		if (it != m_components.end())
		{
			m_components.erase(it);
			return true;
		}

		return false;
	}
}