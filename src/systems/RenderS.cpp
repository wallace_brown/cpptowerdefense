#include "RenderS.h"

#include "RenderC.h"

namespace ecs
{
	//---------------------------------------
	void RenderS::Init()
	{}

	//---------------------------------------
	void RenderS::Tick()
	{
		for (auto& component : RenderC::GetList())
		{
			if (component == nullptr)
			{
				continue;
			}

			component->Tick();
		}
	}

	//---------------------------------------
	void RenderS::Shutdown()
	{}
}