#pragma once

namespace game
{
	void Init();

	bool Tick();

	void Shutdown();
}