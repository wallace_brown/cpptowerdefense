#pragma once

#include "../interfaces/ISystem.h"

namespace ecs
{
	class RenderS : public ISystem
	{
	public:
		virtual void Init();
		virtual void Tick();
		virtual void Shutdown();
	};
}