#pragma once

#include <memory>
#include <vector>

namespace utility
{
	template <typename T>
	class Autolist
	{
	private:
		static std::vector<T*> list;

	public:
		Autolist()
		{
			list.push_back(this);
		}

		~Autolist()
		{
			auto it = std::find(list.begin(), list.end(), this);
			if (it != list.end())
			{
				list.erase(it);
			}
		}

		static std::vector<T*>& GetList();
	};

	template <typename T>
	std::vector<T*> Autolist<T>::list;

	template <typename T>
	std::vector<T*>& Autolist<T>::GetList()
	{
		return list;
	}
}