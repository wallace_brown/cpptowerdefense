#include "../systems/Game.h"

int main()
{
	game::Init();

	while(game::Tick())
	{}

	game::Shutdown();

	return 0;
}