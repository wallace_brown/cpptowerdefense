#pragma once

namespace ecs
{
	class ISystem
	{
	public:
		virtual void Init() = 0;
		virtual void Tick() = 0;
		virtual void Shutdown() = 0;
	};
}