#pragma once

class IComponent
{
public:
	virtual void Init() = 0;
	virtual void Tick() = 0;
	virtual void Destroy() = 0;
};